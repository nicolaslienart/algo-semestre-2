#include <stdio.h>

int dicho_search(int tab[], int value, int from, int to) {
	printf("passage\n");
	int index = (from + to) / 2;
	if (tab[index] == value) return index;
	if (to < from) return -1;
	if (value > tab[to]) return -1;
	if (value < tab[from]) return -1;
	if (value < tab[index]) {
		to = index - 1;
	}
	else {
		from = index + 1;
	}
	return dicho_search(tab, value, from, to);
}

int main() {
	int tab[10] = {1,2,3,4,5,6,7,8,9,10};
	int numUser;
	int loop = 1;
	while(loop) {
		scanf("%d", &numUser);
		if (numUser == -1) break;
		printf("%d\n", dicho_search(tab, numUser, 0, 9));
	}
	return 0;
}
