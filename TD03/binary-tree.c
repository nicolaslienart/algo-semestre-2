#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 5

#define SMALLER -1
#define ROOT 0
#define GREATER 1

typedef struct Branch Branch;

struct Branch {
	int value;
	Branch* greater;
	Branch* smaller;
};

typedef struct ListNode {
	int value;
	struct ListNode* next;
} ListNode;

typedef struct ChainedList {
	int size;
	ListNode *first;
	ListNode *last;
} ChainedList;

ChainedList *list_init() {
	ChainedList *chainedList = malloc(sizeof(ChainedList));

	if (!chainedList) {
		printf("Failed to create a chained list\n");
		exit(EXIT_FAILURE);
	}

	chainedList->size = 0;
	chainedList->first = NULL;
	chainedList->last = NULL;

	return chainedList;
}

ListNode* list_insert(ListNode* previous, int value) {
	ListNode* newNode = malloc(sizeof(ListNode));

	if (!newNode) {
		printf("Failed to add a Node to list\n");
		exit(EXIT_FAILURE);
	}

	newNode->value = value;
	newNode->next = NULL;

	if (!previous) {
		previous = newNode;
	}
	else {
		previous->next = newNode;
	}

	return newNode;
}

void list_fillWithArray(int array[], int arraySize, ChainedList *list) {
	list->first = list_insert(list->first, array[0]);
	list->last = list->first;

	for (int i = 0; i < arraySize; i++) {
		//list->last = list->last->next;
		(list->size)++;
		if (i == 0) {
			continue;
		}
		list->last = list_insert(list->last, array[i]);
	}
}

int height(Branch* node) {
	if (!node) {
		return 0;
		//return -1;
	}
	int smaller = height(node->smaller);
	int greater = height(node->greater);

	if (smaller > greater) {
		return smaller + 1; 
	}
	else {
		return greater + 1;
	}
}

/*
void branch_rotateRight(Branch** node) {
	if (!(*node)->smaller) {
		return;
	}

	Branch* tempNode = (*node);
	Branch* tempGreater = (*node)->smaller->greater;

	(*node) = (*node)->smaller;
	(*node)->greater = tempNode;
	(*node)->greater->smaller = tempGreater;
	node = (*node);
}
*/

Branch* branch_rotateRight(Branch* node) {
	if (!node->smaller) {
		return node;
	}

	Branch* tempNode = node;
	Branch* tempGreater = node->smaller->greater;

	node = node->smaller;
	node->greater = tempNode;
	node->greater->smaller = tempGreater;
	return node;
}

Branch* branch_rotateLeft(Branch* node) {
	if (!node->greater) {
		return node;
	}

	Branch* tempNode = node;
	Branch* tempSmaller = node->greater->smaller;

	node = node->greater;
	node->smaller = tempNode;
	node->smaller->greater = tempSmaller;
	return node;
}

/*
void branch_rotateLeft(Branch** node) {
	if (!(*node)->greater) {
		return;
	}

	Branch* tempNode = *node;
	Branch* tempSmaller = (*node)->greater->smaller;

	(*node) = (*node)->greater;
	(*node)->smaller = tempNode;
	(*node)->smaller->greater = tempSmaller;
	node = (*node);
}
*/

int tree_allLeaves(int array[], Branch* node, int index) {
	if(!node) {
		return 0;
	}
	if(!(node->smaller) && !(node->greater)) {
		array[index] = node->value;
		index;
		return index;
	}
	else {
		int tempIndex = tree_allLeaves(array, node->smaller, index);
		int tempIndex2 = tree_allLeaves(array, node->greater, tempIndex);
		return tempIndex2;
	}
}

int nodeCount(Branch *node) {
	if (!node) return 0;
	return 1 + nodeCount(node->smaller) + nodeCount(node->greater);
}

int tree_isLeaf(Branch* node) {
	if (node->smaller || node->greater) {
		return 1;
	}
	else {
		return 0;
	}
}

void preorderTravel(Branch* node, int array[], int size, int *index) {

	if (!node) {
		return;
	}

	array[*index] = node->value;
	(*index)++;

	preorderTravel(node->smaller, array, size, index);
	
	preorderTravel(node->greater, array, size, index);
}

void inorderTravel(Branch* node, int array[], int size, int *index) {

	if (!node) {
		return;
	}

	inorderTravel(node->smaller, array, size, index);
	
	array[*index] = node->value;
	(*index)++;

	inorderTravel(node->greater, array, size, index);
}

Branch* branch_equilibriumFix(Branch* node, int heightDiff) {
	if (heightDiff > 1) {
		return branch_rotateLeft(node);
	}
	else if (heightDiff < -1) {
		return branch_rotateRight(node);
	}
	else {
		return node;
	}
}

int branch_equilibriumCheck(Branch* node) {
	int heightSmaller = height(node->smaller);
	int heightGreater = height(node->greater);
	int heightDiff = heightSmaller - heightGreater;

	return heightDiff;
}

void tree_normalInsert(Branch* node, int value) {
	if (value >= node->value) {
		if (!(node->greater)) {
			Branch* newBranch = malloc(sizeof(Branch));
			if (!newBranch) return EXIT_FAILURE;
			newBranch->value = value;
			newBranch->greater = NULL;
			newBranch->smaller = NULL;
			node->greater = newBranch;
		}
		else tree_normalInsert(node->greater, value);
	}
	else {
		if (!(node->smaller)) {
			Branch* newBranch = malloc(sizeof(Branch));
			if (!newBranch) return EXIT_FAILURE;
			newBranch->value = value;
			newBranch->greater = NULL;
			newBranch->smaller = NULL;
			node->smaller = newBranch;
		}
		else tree_normalInsert(node->smaller, value);
	}
}

void tree_EQInsert(Branch* node, int value) {
	int heightDiff = branch_equilibriumCheck(node);
	node = branch_equilibriumFix(node, heightDiff);

	if (value >= node->value) {
		if (!(node->greater)) {
			Branch* newBranch = malloc(sizeof(Branch));
			if (!newBranch) return EXIT_FAILURE;
			newBranch->value = value;
			newBranch->greater = NULL;
			newBranch->smaller = NULL;
			node->greater = newBranch;
		}
		else {
			tree_EQInsert(node->greater, value);
		}
	}
	else {
		if (!(node->smaller)) {
			Branch* newBranch = malloc(sizeof(Branch));
			if (!newBranch) return EXIT_FAILURE;
			newBranch->value = value;
			newBranch->greater = NULL;
			newBranch->smaller = NULL;
			node->smaller = newBranch;
		}
		else {
			tree_EQInsert(node->smaller, value);
		}
	}
}

Branch* tree_init(int value) {
	Branch* root = malloc(sizeof(Branch));
	if (!root) {
		printf("Failed to initialize a tree\n");
		exit(EXIT_FAILURE);
	}

	root->value = value;
	root->greater = NULL;
	root->smaller = NULL;

	return root;
}

void postorderTravel(Branch* node, int array[], int size, int *index) {

	if (!node) {
		return;
	}

	inorderTravel(node->smaller, array, size, index);
	inorderTravel(node->greater, array, size, index);
	
	array[*index] = node->value;
	(*index)++;

}

Branch* tree_fillWithList(ChainedList *list) {
	int value;
	if (!(list->size)) return EXIT_FAILURE;

	Branch *root = tree_init(list->first->value);

	ListNode* listNode = list->first->next;

	while (listNode) {
		value = listNode->value;
		tree_EQInsert(root, value);
		listNode = listNode->next;
	}

	return root;
}

void tree_print(Branch* node, int position) {
	if (!node) {
		//printf("\n");
		return;
	}

	printf("[");
	if (position == SMALLER) {
		printf(" %d ", node->value);
	}
	else if (position == GREATER) {
		printf(" %d ", node->value);
	}
	else if (position == ROOT) {
		printf(" %d ", node->value);
	}
	tree_print(node->smaller, SMALLER);
	tree_print(node->greater, GREATER);
	printf("]");
}

int main() {
	int tabToSort[ARRAY_SIZE] = {4,1,9,6,10};

	int leaves[5];
	
	ChainedList *listToSort = list_init();
	list_fillWithArray(tabToSort, ARRAY_SIZE, listToSort);

	Branch* root = tree_fillWithList(listToSort);

	tree_print(root, ROOT);

	/*
	if (tree_isLeaf(root->smaller->greater)) {
		printf("Not a leaf!\n");
	}
	else {
		printf("A leaf!\n");
	}

	int leafCount = tree_allLeaves(leaves, root, 0);

	for (int i = 0; i <= leafCount; i++) {
		printf("%d) leaf : %d\n", i, leaves[i]);
	}
	*/

	int inorderArray[5];
	int treeSize = ARRAY_SIZE;
	int index = 0;

	inorderTravel(root, inorderArray, treeSize, &index); 
	printf("\nInorder data: \n");
	for (int i = 0; i < 5; i++) {
		printf("%d) %d\n", i, inorderArray[i]);
	}

	int preorderArray[5];
	index = 0;

	preorderTravel(root, preorderArray, treeSize, &index); 
	printf("\nPreorder data: \n");
	for (int i = 0; i < 5; i++) {
		printf("%d) %d\n", i, preorderArray[i]);
	}

	int postorderArray[5];
	index = 0;

	postorderTravel(root, postorderArray, treeSize, &index); 
	printf("\nPostorder data: \n");
	for (int i = 0; i < 5; i++) {
		printf("%d) %d\n", i, postorderArray[i]);
	}

	int treeHeight = height(root);
	printf("height = %d\n", treeHeight);

	root = branch_rotateLeft(root);
	tree_print(root, ROOT);
	root = branch_rotateRight(root);
	tree_print(root, ROOT);

	return 0;
}
