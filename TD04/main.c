#include <stdio.h>

#define ARRAY_SIZE 15

typedef struct Heap {
	int array[ARRAY_SIZE];
	int size;
} Heap;

int leftChild(int nodeIndex) {
	int index = 2 * nodeIndex + 1;
	return index;
}

int rightChild(int nodeIndex) {
	int index = 2 * nodeIndex + 2;
	return index;
}

int largest(Heap* heap, int index) {
	int leftIndex = leftChild(index);
	int rightIndex = rightChild(index);
	if (heap->array[leftIndex] > heap->array[index] && heap->array[leftIndex] > heap->array[rightIndex]) {
		return leftIndex;
	}
	else if (heap->array[rightIndex] > heap->array[index] && heap->array[rightIndex] > heap->array[leftIndex]) {
		return rightChild;
	}
	else {
		return index;
	}
}

void heapify(Heap* heap, int nodeIndex) {
	int index = nodeIndex;
	int largestIndex = largest(heap, index);
	int temp;

	if (largestIndex != index) {
		temp = heap->array[index];
		heap->array[index] = heap->array[largestIndex];
		heap->array[largestIndex] = temp;
		heapify(heap, largestIndex);
	}
}

void insertHeapNode(Heap* heap, int value) {
	int index = heap->size;
	int indexParent = (index - 1) / 2;
	int temp;

	heap->array[index] = value;

	while (index > 0 && heap->array[index] > heap->array[indexParent]) {
		temp = heap->array[index];
		heap->array[index] = heap->array[indexParent];
		heap->array[indexParent] = temp;
		index = indexParent;
		indexParent = (index - 1) / 2;
	}
	heap->size++;
}

void buildHeap(Heap* heap, int array, int size) {
	for (int i = 0; i < size; i++) {
		insertHeapNode(heap, array[i]);	
	}
}

void heapSort(Heap heap, int array) {
	int temp;
	for (int i = heap->size - 1; i >= 0; i--) {
		temp = heap.array[0];
		heap.array[0] = heap.array[i];
		heap.array[i] = temp;

		array[i] = heap.array[0];

		heapify(&heap, 0);
	}
}

int main() {
	Heap heap;
	heap.size = 10;
	heap.array[0] = 20;
	heap.array[1] = 18;
	heap.array[2] = 13;
	heap.array[3] = 12;
	heap.array[4] = 8;
	heap.array[5] = 10;
	heap.array[6] = 11;
	heap.array[7] = 2;
	heap.array[8] = 5;
	heap.array[9] = 3;

	insertHeapNode(&heap, 9);
	printf("%d value: %d\n", 10, heap.array[10]);
}
