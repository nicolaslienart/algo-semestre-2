#include <stdio.h>
#include <stdlib.h>

#define ASCII_CONTENT 256

typedef struct Node {
	char character;
	int frequency;
	struct Node* left;
	struct Node* right;
} Node;

typedef struct Heap {
	Node* array[ASCII_CONTENT];
	int size;
} Heap;

int leftChild(int nodeIndex) {
	int index = 2 * nodeIndex + 1;
	return index;
}

int rightChild(int nodeIndex) {
	int index = 2 * nodeIndex + 2;
	return index;
}

int smallest(Heap* heap, int index) {
	int leftIndex = leftChild(index);
	int rightIndex = rightChild(index);

	// TODO check seg fault left and right children possibly don't exist ?
	if (leftIndex < heap->size && heap->array[leftIndex]->frequency < heap->array[index]->frequency) {
		//if (heap->array[leftIndex]->frequency < heap->array[rightIndex]->frequency) {
			return leftIndex;
		//}
	}
	else if (rightIndex < heap->size && heap->array[rightIndex]->frequency < heap->array[index]->frequency) {
		//if (heap->array[rightIndex]->frequency < heap->array[leftIndex]->frequency) {
			return rightIndex;
		//}
	}
	else {
		return index;
	}
}

void heapify(Heap* heap, int index) {
	int smallestIndex = smallest(heap, index);
	Node* temp;

	if (smallestIndex != index) {
		temp = heap->array[index];
		heap->array[index] = heap->array[smallestIndex];
		heap->array[smallestIndex] = temp;
		heapify(heap, smallestIndex);
	}
}

Node* createNode(char character, int frequency)
{
	Node* node = malloc(sizeof(Node));
	if (!node) {
		exit(EXIT_FAILURE);
	}
	node->character = character;
	node->frequency = frequency;

	node->left = NULL;
	node->right = NULL;

	return node;
}

void heapToMin(Heap* heap) 
{
	for (int i = (heap->size - 2) / 2; i >= 0; --i) {
		heapify(heap, i);
	}
}

void insertHeapNode(Heap* heap, Node* node) 
{
	int index = heap->size;
	int indexParent = (index - 1) / 2;

	Node* temp;

	heap->array[index] = node;

	while (index > 0 && heap->array[index]->frequency < heap->array[indexParent]->frequency) {
		temp = heap->array[index];
		heap->array[index] = heap->array[indexParent];
		heap->array[indexParent] = temp;
		index = indexParent;
		indexParent = (index - 1) / 2;
	}

	heap->size++;
}

Node* huffmanExtract(Heap* heap) 
{
	int heapSize = heap->size;

	Node* temp = heap->array[0];
	heap->array[0] = heap->array[heapSize -1];

	heap->size--;
	heapify(heap, 0);

	return temp;
}

Node* huffmanDict(Heap* heap) 
{
	int sumFreq;
	Node* parent;
	Node* leftChild;
	Node* rightChild;

	while (heap->size != 1) {
		leftChild = huffmanExtract(heap);
		rightChild = huffmanExtract(heap);

		sumFreq = leftChild->frequency + leftChild->frequency;

		parent = createNode('\0', sumFreq);

		parent->left = leftChild;
		parent->right = rightChild;

		insertHeapNode(heap, parent);
	}

	return huffmanExtract(heap);
}

Heap* huffmanHeap(int frequences[]) 
{
	Heap* heap = malloc(sizeof(Heap));
	if (!heap) {
		exit(EXIT_FAILURE);
	}
	int size = 0;

	for (int i = 0; i < ASCII_CONTENT; i++) {
		if (frequences[i] != 0) {
			heap->array[size] = createNode(i, frequences[i]);
			size++;
		}
	}

	heap->size = size;

	heapToMin(heap);

	return heap;
}

void charFrequences(char string[], int frequences[]) 
{
	int index = 0;
	char currentChar;

	while (string[index] != '\0') {
		currentChar = (int) string[index];
		frequences[currentChar]++;
		index++;
	}
}

void huffmanTranslate(char *dictionary[], char character, char binaryCode[], int size) 
{
	char* charCode = malloc(sizeof(char) * (size+1));
	if (!charCode) {
		exit(EXIT_FAILURE);
	}
	int i;

	for (i = 0; i < size; i++) {
		charCode[i] = binaryCode[i];
	}

	charCode[i] = '\0';

	dictionary[(int) character] = charCode;
}

void huffmanBuildDictionary(Node* dict, char binaryCode[], char *dictionary[], int size, int* totalSize) 
{
	int index = size;
	if (dict->left) {
		binaryCode[index] = '0';
		huffmanBuildDictionary(dict->left, binaryCode, dictionary, size +1, totalSize);
	}
	if (dict->right) {
		binaryCode[index] = '1';
		huffmanBuildDictionary(dict->right, binaryCode, dictionary, size +1, totalSize);
	}
	if (!dict->left && !dict->right) {
		huffmanTranslate(dictionary, dict->character, binaryCode, size);
		*totalSize += size;	
	}
}

char* huffmanEncode(Node* dict, char input[])
{
	char* dictionary[ASCII_CONTENT] = {NULL};
	int size = 0;

	char str[ASCII_CONTENT];
	huffmanBuildDictionary(dict, str, dictionary, 0, &size); 

	char* output = malloc(sizeof(char) * (size+1));
	if (!output) {
		exit(EXIT_FAILURE);
	}

	int indexInputChar = 0;
	int indexOutputChar = 0;
	int indexDictChar = input[indexInputChar];
	int indexDictBin = 0;

	while (indexDictChar != '\0') {

		indexDictBin = 0;
		while (dictionary[indexDictChar][indexDictBin] != '\0') {
			output[indexOutputChar] = dictionary[indexDictChar][indexDictBin];
			indexOutputChar++;
			indexDictBin++;
		}

		indexInputChar++;
		indexDictChar = input[indexInputChar];
	}

	output[indexOutputChar] = '\0';

	return output;
}

char* huffmanDecode(Node* dict, char input[])
{
	char* output = malloc(sizeof(char) *400);
	int currentChar = 0;
	Node* node = dict;

	int i = 0;
	while (input[i] != '\0') {
		while (node->left || node->right) {
			if (input[i] == '0') {
				node = node->left;
			}
			else if (input[i] == '1') {
				node = node->right;
			}
			i++;
		}

		output[currentChar] = node->character;
		currentChar++;
		node = dict;
	}
	output[currentChar] = '\0';
	return output;
}

int main() 
{
	char string[] = "zqdqzdqzd999955zqdqd44d4q5dqD44";
	int frequences[ASCII_CONTENT] = {0};


	charFrequences(string, frequences);

	Heap* heap = huffmanHeap(frequences);

	Node* root = huffmanDict(heap);

	int size = 0;
	char* encoded = huffmanEncode(root, string);

	for (int i = 0; i < 256; i++) {
		printf("%c -> %d\n", i, frequences[i]);
	}
	
	for (int i = 0; encoded[i] != '\0'; i++) {
		printf("%c", encoded[i]);
	}
	printf("\n");


	char* decoded = huffmanDecode(root, encoded);

	for (int i = 0; decoded[i] != '\0'; i++) {
		printf("%c", decoded[i]);
	}
	printf("\n");
}
