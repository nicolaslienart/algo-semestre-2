#include <stdio.h>
#include <stdlib.h>

#define SIZE 5

void array_print(int array[], int size) {
	printf("Printing array\n");
	for (int i = 0; i < size; i++) {
		printf("array[ %02d ] = %d\n", i, array[i]);
	}
}


void merge(int array[], int first, int middle, int last) {
	int i, j, k;
	int size1 = middle - first + 1;
	int size2 = last - middle;

	int *firstArray = malloc(size1 * sizeof(int));
	int *secondArray = malloc(size2 * sizeof(int));

	if (!firstArray || !secondArray) {
		return EXIT_FAILURE;
	}

	for (i = 0; i < size1; i++) {
		firstArray[i] = array[first + i];
	}

	for (j = 0; j < size2; j++) {
		secondArray[j] = array[middle + 1 + j];
	}

	i = j = 0;
	k = first;

	while (i < size1 && j < size2) {

		if (firstArray[i] <= secondArray[j]) {
			array[k] = firstArray[i];
			i++;
		}
		else {
			array[k] = secondArray[j];
			j++;
		}
		k++;

	}

	for (; i < size1; i++) {
		array[k] = firstArray[i];
		k++;
	}

	for (; j < size2; j++) {
		array[k] = secondArray[j];
		k++;
	}
}

void recursiveMerge(int array[], int first, int last) {

	if (first < last) {
		int middle = first + (last - first) / 2;
		recursiveMerge(array, first, middle);
		recursiveMerge(array, middle + 1, last);
		merge(array, first, middle, last);
	}

}

void sort_merge(int array[], int size) {

	recursiveMerge(array, 0, size - 1);

}

int main() {

	int array[SIZE] = {22,25,12,87,2};

	sort_merge(array, SIZE);

	array_print(array, SIZE);

	return 0;

}
