#include <stdio.h>

#define SIZE 5

void array_print(int array[], int size) {
	printf("Printing array\n");
	for (int i = 0; i < size; i++) {
		printf("array[ %02d ] = %d\n", i, array[i]);
	}
}

void sort_insertion(int array[], int size) {
	int index;
	int temp;

	for (int i = 1; i < size; i++) {
		temp = array[i];
		for (index = i - 1; array[index] >= temp; index--) {
			if (index < 0) break;
			array[index + 1] = array[index];
		}
		array[index+1] = temp;
	}
}

int main() {
	int array[SIZE] = {22,25,12,87,2};

	sort_insertion(array, SIZE);

	array_print(array, SIZE);
	return 0;
}
