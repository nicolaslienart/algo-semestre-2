#include <stdio.h>

#define SIZE 5

void array_print(int array[], int size) {
	printf("Printing array\n");
	for (int i = 0; i < size; i++) {
		printf("array[ %02d ] = %d\n", i, array[i]);
	}
}

void sort_selection(int array[], int size) {
	int minIndex;
	int temp;

	for (int i = 0; i < size; i++) {
		minIndex = i;
		for (int j = i; j < size; j++) {
			if (array[j] < array[minIndex]) {
				minIndex = j;
			}
		}
		temp = array[i];
		array[i] = array[minIndex];
		array[minIndex] = temp;
	}
}

int main() {
	int array[SIZE] = {22,25,12,87,2};
	sort_selection(array, SIZE);

	array_print(array, SIZE);
	return 0;
}
