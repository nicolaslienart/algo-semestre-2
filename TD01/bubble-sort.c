#include <stdio.h>

#define SIZE 5

void array_print(int array[], int size) {
	printf("Printing array\n");
	for (int i = 0; i < size; i++) {
		printf("array[ %02d ] = %d\n", i, array[i]);
	}
}

void sort_bubble(int array[], int size) {
	int end = size - 1;
	int temp;

	for (int i = 0; i < end; i++) {
		for (int j = 0; j < end ; j++) {
			if (array[j] > array[j + 1]) {
			temp = array[j];
			array[j] = array[j + 1];
			array[j + 1] = temp;
			}
		}
	}
}

int main() {
	int array[SIZE] = {22,25,12,87,2};

	sort_bubble(array, SIZE);

	array_print(array, SIZE);
	return 0;
}
