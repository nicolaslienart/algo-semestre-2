#include <iostream>

using namespace std;

double pow(double num, int power) {
	double total = num;
	bool negative = false;
	if (power < 0) {
		negative = true;
		power = -power;
	} 
	else if (power == 0) return 1;
	while(--power) {
		total *= num;
	}
	if (negative) return 1/total;
	else return total;
}

int recurPow(int value, int n) {
	if (!n) return 1;
	else return value * recurPow(value, n-1);
}

int main() {
	double myNum = 2;
	cout << pow(myNum, 2) << endl;
	cout << recurPow(myNum, 2) << endl;
	return 0;
}
