#include <iostream>
using namespace std;

int fibonacci(int n) {
	if (!n) return 1;
	else return n * fibonacci(n-1);
}

int main() {
	cout << fibonacci(3) << endl;
	return 0;
}
