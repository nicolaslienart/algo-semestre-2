#include <iostream>
using namespace std;

int allEvens(int evens[], int array[], int evenSize, int arraySize) {
	int arrayIndex = --arraySize;
	if (arrayIndex < 0) return evenSize;
	if ((array[arrayIndex] % 2) == 0) {
		evens[evenSize] = array[arrayIndex];
		evenSize++;
	}
	return allEvens(evens, array, evenSize, arrayIndex);
}

int main() {
	int evens[8] = {0};
	int array[8] = {0,2,3,4,5,6,7,10};
	//cout << allEvens(evens, array, 0, 5) << endl;
	for (int i = 0; i < allEvens(evens, array, 0, 8); ++i) {
		cout << i << ") ";
		cout << evens[i] << endl;
	}
	return 0;
}
