#include <stdio.h>

#define SIZE 5

void array_print(int array[], int size) {
	printf("Printing array\n");
	for (int i = 0; i < size; i++) {
		printf("array[ %02d ] = %d\n", i, array[i]);
	}
}

void sort_quick(int array[], int size) {
	int pivot;
	int i;
	int temp;
	int end;

	if (size < 2) {
		return;
	}

	pivot = size - 1;
	end = 0;
	for (int i = 0; i < size; i++) {
		if (array[i] <= array[pivot]) {
			if (end != i) {
				temp = array[i];
				array[i] = array[end];
				array[end] = temp;
			}
			end++;
		}
	}
	sort_quick(array, end - 1);
	sort_quick(array + end - 1, size - end + 1);
}

int main() {
	int array[SIZE] = {22,25,12,87,2};

	sort_quick(array, SIZE);

	array_print(array, SIZE);
	return 0;
}
