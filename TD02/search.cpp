#include <iostream>

using namespace std;

int search(int value, int array[], int size) {
	if (size < 0) return -1;
	if (value == array[size-1]) return size-1;
	else return search(value, array, size-1);
}

int main() {
	int const maxArray(5);
	int array[maxArray] = {1,2,3,4,5};

	cout << search(5, array, maxArray) << endl;
	return 0;
}
