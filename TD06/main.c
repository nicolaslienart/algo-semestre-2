#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
  int value;
  struct Node* child;
} Node;

typedef struct Graph {
  int size;
  Node* vertices[];
} Graph;

Node* node_init(int value)
{
  Node* node = malloc(sizeof(Node));
  if (!node) {
    return EXIT_FAILURE;
  }

  node->value = value;
  node->child = NULL;

  return node;
}

node_addToList(Node* node, int value)
{
  while (node->child) {
    node = node->child;
  }

  node->child = node_init(value);
}

Graph* BuildFromrAdjenciesMatrix(int nodeCount, int adjencies[][nodeCount])
{
  Graph* graph = malloc(sizeof(Graph));
  if (!graph) {
    return EXIT_FAILURE;
  }
  graph->size = nodeCount;

  Node* vertices = malloc(sizeof(Node) * nodeCount);
  if (!vertices) {
    return EXIT_FAILURE;
  }

  for (int i=0; i<nodeCount; i++) {
    graph->vertices[i] = node_init(i);

    for (int j=0; j<nodeCount; j++) {
      if (adjencies[i][j]) {
        node_addToList(graph->vertices[i], j);
      }
    }
  }

  return graph;
}

void deepTravel(Node* first, Node* nodes[], int* nodeSize, char visited[])
{
  visited[first->value] = 1;
  printf("Node %d\n", first->value);

  Node* node = first->child;

  while (node) {
    if (!visited[node->value]) {

      if (nodes) {
        nodes = realloc(nodes, sizeof(Node *) * *nodeSize);
      }
      else {
        nodes = malloc(sizeof(Node *));
      }
      if (!nodes) {
        return EXIT_FAILURE;
      }

      nodes[*nodeSize] = node;

      *nodeSize += 1;
      deepTravel(node, nodes, nodeSize, visited);
    }

    node = node->child;
  }
}

int main()
{
  int adjencies[4][4] = {
    {0, 1, 1, 1},
    {1, 0, 1, 1},
    {1, 1, 0, 1},
    {1, 1, 1, 0},
  };
  Graph* myGraph = BuildFromrAdjenciesMatrix(4, adjencies);

  Node** nodes = NULL;
  int nodeSize = 0;
  char visited[4] = {0};
  deepTravel(myGraph->vertices[0], nodes, &nodeSize, visited);
}
