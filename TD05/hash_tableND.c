#include <stdio.h>
#include <stdlib.h>

#define ASCII 256

typedef enum Bool {
  FALSE,
  TRUE
} Bool;

typedef struct StrList {
  char* string;
  struct StrList* next;
} StrList;

int findHash(char string[])
{
  int hash;
  if (string[0] >= 256) {
    hash = string[0] / ASCII;
  }
  else {
    hash = string[0];
  }
  return hash;
}

Bool contains(StrList* hash_table[], char string[])
{
  int hash = findHash(string);
  int i;

  if (hash_table[hash]) {
    StrList* node = hash_table[hash];

    while (node) {
      i = 0;
      for (; node->string[i] != '\0'; i++) {
        if (node->string[i] != string[i]) break;
      } 

      if (node->string[i] == string[i]) return TRUE;

      node = node->next;
    }

    return FALSE;
  }

  return FALSE;
}

char* copyStr(char string[]) 
{
  int length = 0;

  do {
    length++;
  } while (string[length] != '\0');

  char* strCopy = malloc(sizeof(char) * length);
  if (!strCopy) return EXIT_FAILURE;

  for (int i = 0; i < length; i++) {
    strCopy[i] = string[i];
  }

  return strCopy;
}

void addElement(StrList* hash_table[], int hash, char string[]) 
{
  StrList* rootStr = hash_table[hash];

  StrList* node = malloc(sizeof(StrList));
  if (!node) return EXIT_FAILURE;

  node->string = copyStr(string);
  node->next = NULL;

  if (rootStr) {
    StrList* nodeTravel = rootStr;

    while (nodeTravel->next) {
      nodeTravel = nodeTravel->next;
    }
    nodeTravel->next = node;
  }
  else hash_table[hash] = node;
}

void insert(StrList* hash_table[], char string[])
{
  int hash = findHash(string);
  addElement(hash_table, hash, string);
}

int main()
{
  char string1[] = "alejd";
  char string2[] = "a214";
  char string3[] = "dqzhdj";
  StrList* hash_table[ASCII] = {NULL};


  insert(hash_table, string1);
  insert(hash_table, string2);
  insert(hash_table, string3);

  if (contains(hash_table, "alejda")) {
    printf("Ahahah\n");
  }
}
