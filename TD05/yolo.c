#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 40

int hash(char* string)
{
  int hash = 0;
  for (int i = 0; string[i] != '\0'; i++) {
    hash += string[i]; }
  return hash % ARRAY_SIZE;
}

void copyStringToHash(char *hash_table[], char string[])
{
  int sizeStr;
  for (sizeStr= 0; string[sizeStr] != '\0'; ++sizeStr);

  char* store = malloc(sizeof(char) * (sizeStr + 1));
  if (!store) exit(EXIT_FAILURE);

  int idx = 0;
  do {
    store[idx] = string[idx]; 
    idx++;
  } while (string[idx] != '\0');

  hash_table[hash(string)] = store;
}

int main()
{
  char string[] = "Yolo";
  char*  hash_table[ARRAY_SIZE];
  copyStringToHash(hash_table, string);
  printf("Content of the hash_table at %d is %s\n", hash(string), string);
}
