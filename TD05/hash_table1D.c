#include <stdio.h>
#include <stdlib.h>

#define ASCII 256

typedef enum Bool {
  FALSE,
  TRUE
} Bool;

int findHash(char string[])
{
  int hash;
  if (string[0] >= 256) {
    hash = string[0] / ASCII;
  }
  else {
    hash = string[0];
  }
  return hash;
}

Bool contains(char* hash_table[], char string[])
{
  int hash = findHash(string);

  if (hash_table[hash]) {
    for (int i = 0; hash_table[hash][i] != '\0'; i++) {
      if (hash_table[hash][i] != string[i]) return FALSE;
    } 

    return TRUE;
  }

  return FALSE;
}

char* copyStr(char string[]) 
{
  int length = 0;

  do {
    length++;
  } while (string[length] != '\0');

  char* strCopy = malloc(sizeof(char) * length);
  if (!strCopy) return EXIT_FAILURE;

  for (int i = 0; i < length; i++) {
    strCopy[i] = string[i];
  }

  return strCopy;
}

void insert(char* hash_table[], char string[])
{
  int hash = findHash(string);
  if (hash_table[hash]) free(hash_table[hash]);
  hash_table[hash] = copyStr(string);
}

int main()
{
  char string1[] = "alejd";
  char string2[] = "a214";
  char string3[] = "dqzhdj";
  char* hash_table[ASCII] = {NULL};


  insert(hash_table, string1);
  insert(hash_table, string2);
  insert(hash_table, string3);

  if (contains(hash_table, string1)) {
    printf("Ahahah\n");
  }
}
